import pathlib
import os
import csv
import argparse
import sys

def get_mkv_list(file_list):
    mkv_list = []
    for i in file_list:
        if pathlib.Path(i).suffix == ".mkv":
            mkv_list.append(i)
    return mkv_list

def strip_invalid_chars(file_name,replacementchar=''):
    invalid_chars = ['\\', '/', ':', '*', '?', '"', ">", "<", "|"]

    for char in invalid_chars:
        file_name = file_name.replace(char, replacementchar)
    
    return file_name


def main():
    parser = argparse.ArgumentParser(
        prog="rename_series.py",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='''\
            Utility for renaming seasons of TV series in a format Plex expects.
            All files should be in a single directory, sorting by name in the episode order.
            
            A csv file in the same directory must be in the format of 
            season number, episode number, episode title
            ''',
            epilog=" "
    )
    parser.add_argument("-c", "--csv", type=str, help="Define csv file location.")
    parser.add_argument("-d", "--dir", type=pathlib.Path, help="Directory containing the files to be renamed and configured csv.", default=".")
    parser.add_argument("-s", "--show", type=str, help="Specify the show name. Required.")
    parser.add_argument("-r", "--replacementchar", type=str, help="Specify a character to replace invalid characters in title (\, /, :, *, ?, \", >, <, |) - defaults to '' ", default="")
    parser.add_argument("--dry", help="Just prints what the rename would look like, does not change file names.", action="store_true")
    args = parser.parse_args()

    if not args.csv:
        sys.exit("ERROR: A csv containing season number, episode number, title is required. Use -c to specify the file.")
    if not args.show:
        sys.exit("ERROR: Must supply a show name with -s.")
    if not os.path.exists(args.csv):
        sys.exit(f"ERROR: Supplied csv, '{args.csv}' does not exist.")    
    if args.replacementchar:
        invalid_chars = ['\\', '/', ':', '*', '?', '"', ">", "<", "|"]
        for char in invalid_chars:
            if char in args.replacementchar:
                sys.exit(f"ERROR: supplied replacement character '{args.replacementchar}' is in the invalid character list: (\, /, :, *, ?, \", >, <, |)")
    if args.dir:
        if not os.path.exists(args.dir):
            sys.exit(f"ERROR: Supplied path, '{args.dir}' does not exist.")

    with open(args.csv, "r") as csv_file:
        reader = csv.reader(csv_file)
        rows = list(reader)

        file_list = get_mkv_list(os.listdir(args.dir))

        if len(file_list) != len(rows):
            sys.exit(f"ERROR: The entries in csv file ({len(rows)}) does not match number of mkv files ({len(file_list)})")

        for episode in range(len(file_list)):
            file_name = file_list[episode]
            episode_row = rows[episode]
            season_number = int(episode_row[0])
            episode_number = int(episode_row[1])
            episode_title = episode_row[2]

            episode_title = strip_invalid_chars(episode_title,args.replacementchar)

            new_file_name = f"{args.show} - s{season_number:02d}e{episode_number:02d} - {episode_title}.mkv"

            source_file = os.path.join(args.dir, file_name)

            dest_file = os.path.join(args.dir, new_file_name)

            print(f"Renaming: {file_name} -> {new_file_name}")

            if not args.dry:
                os.rename(source_file, dest_file)


        # for row in rows:
        #     for file in os.listdir(read_dir):
        #         print(f"{file} -> {args.show} - s{row[0]:02d}e{row[1]:02d} - {row[2]}")


        # for row in rows:
        #     print(row)

if __name__ == "__main__":
    main()